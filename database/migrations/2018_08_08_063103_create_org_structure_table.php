<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('org_structure', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('parent_id');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::drop("org_structure");
    }

}
