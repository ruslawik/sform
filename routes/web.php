<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
	//Роуты фронтухи
	Route::get('', 'indexController@getIndex');
	Route::get('/shop', 'indexController@getShop');
	Route::get('/single', 'indexController@getSingle');

	Route::any('/all', 'indexController@getAllProd');
	Route::post('/login2', 'LoginController@getLogin');
	Route::post('/login', 'LoginController@postLogin');
	Route::any('/logout', 'LoginController@getLogout');

	//Защита админки посредником через роль аккаунта из БД (админ - 1, юзер - 2)
	Route::group(['middleware' => ['auth.superlevel'], 'prefix' => 'superlevel'], function () {
		Route::any('/main', 'SuperLevel\SuperLevelController@getIndex');
		Route::any('/add_user', 'SuperLevel\SuperLevelController@getAddUser');
		Route::any('/save_user', 'SuperLevel\SuperLevelController@savePostNewUser');
		Route::any("/all_users", 'SuperLevel\SuperLevelController@getAllUsers');

	});

	//Для учителя через посредник, проверяющий на роль поупателя и авторизацию
	Route::group(['middleware' => ['auth.secondlevel'], 'prefix' => 'secondlevel'], function () {
		Route::any('/main', 'SecondLevel\SecondLevelController@getIndex');
		Route::any('/cabinet', 'SecondLevel\SecondLevelController@getCabinet');

	});

	//Для обычного юзера роуты через простой посредник, проверяющий только авторизацию
	Route::group(['middleware' => ['auth.user'], 'prefix' => 'user'], function () {
		Route::any('main', 'User\UserController@getIndex');

	});