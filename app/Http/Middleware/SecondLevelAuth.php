<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;

class SecondLevelAuth {
    protected $auth;

    function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next){
        if ($this->auth->guest())
            return redirect()->action('indexController@getIndex')->with('error', 'Введите логин и пароль для доступа');
        if(Auth::user()->user_type!=2)
        	return redirect()->action('indexController@getIndex')->with('error', 'Вы не можете иметь доступ второго уровня к панели');
        return $next($request);
    }
}
