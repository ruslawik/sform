<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use App\User;

class indexController extends Controller
{
    public function getIndex(){

        $ar['login'] = "";
    	return view("user.index", $ar);
    }

    public function getShop(){

    	$ar['login'] = "";

    	return view("user.shop", $ar);
    }

    public function getSingle(){

    	$ar['login'] = "";

    	return view("user.single", $ar);
    }

    public function getAllProd(){

        $response = \Httpful\Request::post("http://k2erp.dyndns.org:9981/api/authory/login")
        ->body('"b95b5fdacd1ddb6e9d49619af71dc774"')
        ->sendsJson()
        ->addHeaders(array('ApplicationToken' => '7E7265561B304D80A8BC0AF7EF28F09F'))
        ->send();

        $access_token = preg_replace('/"/', '', $response);

        $all_prod = \Httpful\Request::get("http://k2erp.dyndns.org:9981/api/objects/product/all")
        ->sendsJson()
        ->addHeaders(array('ApplicationToken' => '7E7265561B304D80A8BC0AF7EF28F09F',
                            'AccessToken' => $access_token))
        ->send();
        
        print("<pre>");
        $ar = json_decode($all_prod);
        print_r($ar);
        print("</pre>");

    }

}
