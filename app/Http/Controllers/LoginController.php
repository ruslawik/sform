<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Application;
use Hash;
use Auth;

class LoginController extends Controller{
    function getLogin (){
                /*
        $ar['login'] = "";
        $user = new User();
        $user->email = "admin";
        $user->user_type = 1;
        $user->password = Hash::make('346488');
        $user->name = "Admin";
        $user->surname = "Admin";
        $user->save();
        */
        $ar = array();
        $ar['action'] = action('LoginController@postLogin');
        
        return view('superlevel.login', $ar);
    }

    function postLogin(Request $request){
        if (!Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')]))
            return back()->with('error', 'Неверный логин/пароль');

        if (Auth::user()->user_type == 1)
            return redirect()->action('SuperLevel\SuperLevelController@getIndex');
        else if (Auth::user()->user_type == 2)
            return redirect()->action('SecondLevel\SecondLevelController@getIndex');
        else if (Auth::user()->user_type == 3)
            return redirect()->action('User\UserController@getIndex');
        else abort(404);
    }

    function getLogout(){
        Auth::logout();

        return redirect()->to('/');
    }
}
