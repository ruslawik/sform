<?php
namespace App\Http\Controllers\SuperLevel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Application;
use Hash;
use Auth;
use Session;
use Validator;

class SuperLevelController extends Controller {

    function getIndex (){
        $ar = array();
        $ar['title'] = "Панель управления";
        $ar['username'] = Auth::user()->name;

        return view('superlevel.main', $ar);
    }

    function getAddUser (){
        $ar = array();
        $ar['title'] = 'Добавить нового пользователя';
        $ar['action'] = action('SuperLevel\SuperLevelController@savePostNewUser');

        return view("superlevel.users.add_user", $ar);

    }

    function getAllUsers (){
        $ar = array();
        $ar['title'] = 'Все пользователи';
        $ar['users'] = User::all();

        return view("superlevel.users.all_users", $ar);
    }

    function savePostNewUser (Request $request){
        $validatedData = $request->validate([
            'email' => 'required|unique:users|max:255',
            'name' => 'required',
            'surname' => 'required',
            'password' => 'required'
        ]);

        $new_user = new User;
        $new_user->email = $request->input('email');
        $new_user->name = $request->input('name');
        $new_user->surname = $request->input('surname');
        $new_user->user_type = $request->input('role');
        $new_user->password = Hash::make($request->input('password'));
        $new_user->save();
            
        return back();
    }

}
