<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class apiController extends Controller
{
    public function update($catalog_name, $object_id, $token){

    	if($token != "7E51CE657D6918E7BE54C6C922FA7"){
    		$ar["status"] = "Invalid API token";
    	}else{
    		$ar['status'] = "success";
    	}

    	$json = json_encode($ar);
    	return response($json, 200)
    					->header('Content-Type', 'application/json');
    }

}
