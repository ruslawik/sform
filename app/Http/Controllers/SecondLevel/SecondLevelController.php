<?php

namespace App\Http\Controllers\SecondLevel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use Carbon\Carbon;
use Validator;

class SecondLevelController extends Controller
{
    public function getIndex (){
        $ar = array();
        $ar['title'] = "Advicer";
        $ar['name'] = Auth::user()->name." ".Auth::user()->surname;

        return view('user.index', $ar);
    }   

    public function getCabinet(){

    	$ar['name'] = Auth::user()->name." ".Auth::user()->surname;


    	return view('user.cabinet', $ar);
    }

}
