<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use Validator;


class UserController extends Controller
{
 	public function getIndex (){
        $ar = array();
        $ar['title'] = "Advicer";
        $ar['name'] = Auth::user()->name." ".Auth::user()->surname;

        return view('user.index', $ar);
    }   

}
