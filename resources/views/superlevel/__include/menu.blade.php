<ul class="nav navbar-nav">
                    <li class="active">
                        <a href="/superlevel/main"> <i class="menu-icon fa fa-dashboard"></i>Главная</a>
                    </li>

                    <h3 class="menu-title">Пользователи</h3><!-- /.menu-title -->
                        <li><a href="/superlevel/all_users"><i class="menu-icon fa fa-list"></i>Все</a></li>
                        <li><a href="/superlevel/add_user"><i class="menu-icon fa fa-plus"></i>Добавить</a></li>

                    <h3 class="menu-title">Товары</h3><!-- /.menu-title -->
                        <li><a href="/superlevel/all_products"><i class="menu-icon fa fa-list"></i>Все товары</a></li>
                        <li><a href="/superlevel/all_cats"><i class="menu-icon fa fa-list-alt"></i>Категории</a></li>

                    <h3 class="menu-title">Заказы</h3><!-- /.menu-title -->
                        <li><a href="/superlevel/all_orders"><i class="menu-icon fa fa-tasks"></i>Все заказы</a></li>

                </ul>