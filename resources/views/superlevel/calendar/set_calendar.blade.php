@extends('manage.layout')

@section('title', $title)

@section('content')
    
     @if ($errors->any())
    <div class="alert alert-danger">
        <ul style="margin-left:20px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Расписание</span> Установите расписание для учителя {{ $teacher[0]->name }} {{ $teacher[0]->surname }} - {{ $teacher[0]->login }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form action="{{ $action }}" method="POST">
                {{ csrf_field() }}
            <input type="hidden" name="for_teacher" value="{{ $teacher[0]->id }}">
            <table class="table">
            <tr>
                <td>Выберите время:</td>
                <td>Название предмета:</td>
                <td>День недели</td>
            </tr>
            <tr>
                <td>
                    <select name="hours" class="form-control col-sm-4" style="float:left;">
                        @for($i = 7; $i<=22; $i++)
                            <option value="{{$i}}">{{ $i<10?"0":"" }}{{$i}}</option>
                        @endfor
                    </select>
                    <select name="minutes" class="form-control col-sm-4" style="float:left;">
                        @for($i = 0; $i<=59; $i++)
                            <option value="{{$i}}">{{ $i<10?"0":"" }}{{$i}}</option>
                        @endfor
                    </select>
                </td>
                <td>
                    <input type='text' name="predmet_name" class="form-control">
                </td>
                <td>
                     <select name="dni" class="form-control">
                        <option value="1">Пн</option>
                        <option value="2">Вт</option>
                        <option value="3">Ср</option>
                        <option value="4">Чт</option>
                        <option value="5">Пт</option>
                        <option value="6">Сб</option>
                        <option value="7">Вс</option>
                     </select>
                </td>
                <td>
                    <input type="submit" value="Добавить" class="btn btn-success">
                </td>
            </tr>
           </table>
        </form>
    </div>

    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">Понедельник</strong>
                            </div>
                            <div class="card-body">
                                <div class="mx-auto d-block">
                                    @foreach($list as $element)
                                        @if($element->apta_kuni === 1)
                                            {{$element->hours<10?"0":""}}{{ $element->hours }}:{{$element->minutes<10?"0":""}}{{ $element->minutes }} - {{ $element->predmet_name }} <a href="/manage/calendar/delete/{{ $element->id }}"><i class='fa fa-trash'></i></a><br>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
            </div>
            <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">Вторник</strong>
                            </div>
                            <div class="card-body">
                                <div class="mx-auto d-block">
                                    @foreach($list as $element)
                                        @if($element->apta_kuni === 2)
                                            {{$element->hours<10?"0":""}}{{ $element->hours }}:{{$element->minutes<10?"0":""}}{{ $element->minutes }} - {{ $element->predmet_name }} <a href="/manage/calendar/delete/{{ $element->id }}"><i class='fa fa-trash'></i></a><br>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
            </div>

            <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">Среда</strong>
                            </div>
                            <div class="card-body">
                                <div class="mx-auto d-block">
                                    @foreach($list as $element)
                                        @if($element->apta_kuni === 3)
                                            {{$element->hours<10?"0":""}}{{ $element->hours }}:{{$element->minutes<10?"0":""}}{{ $element->minutes }} - {{ $element->predmet_name }} <a href="/manage/calendar/delete/{{ $element->id }}"><i class='fa fa-trash'></i></a><br>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
            </div>
             <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">Четверг</strong>
                            </div>
                            <div class="card-body">
                                <div class="mx-auto d-block">
                                    @foreach($list as $element)
                                        @if($element->apta_kuni === 4)
                                            {{$element->hours<10?"0":""}}{{ $element->hours }}:{{$element->minutes<10?"0":""}}{{ $element->minutes }} - {{ $element->predmet_name }} <a href="/manage/calendar/delete/{{ $element->id }}"><i class='fa fa-trash'></i></a><br>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
            </div>
        <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">Пятница</strong>
                            </div>
                            <div class="card-body">
                                <div class="mx-auto d-block">
                                    @foreach($list as $element)
                                        @if($element->apta_kuni === 5)
                                            {{$element->hours<10?"0":""}}{{ $element->hours }}:{{$element->minutes<10?"0":""}}{{ $element->minutes }} - {{ $element->predmet_name }} <a href="/manage/calendar/delete/{{ $element->id }}"><i class='fa fa-trash'></i></a><br>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
            </div>
            <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">Суббота</strong>
                            </div>
                            <div class="card-body">
                                <div class="mx-auto d-block">
                                    @foreach($list as $element)
                                        @if($element->apta_kuni === 6)
                                            {{$element->hours<10?"0":""}}{{ $element->hours }}:{{$element->minutes<10?"0":""}}{{ $element->minutes }} - {{ $element->predmet_name }} <a href="/manage/calendar/delete/{{ $element->id }}"><i class='fa fa-trash'></i></a><br>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
            </div>

            <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">Воскресенье</strong>
                            </div>
                            <div class="card-body">
                                <div class="mx-auto d-block">
                                    @foreach($list as $element)
                                        @if($element->apta_kuni === 7)
                                            {{$element->hours<10?"0":""}}{{ $element->hours }}:{{$element->minutes<10?"0":""}}{{ $element->minutes }} - {{ $element->predmet_name }} <a href="/manage/calendar/delete/{{ $element->id }}"><i class='fa fa-trash'></i></a><br>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
            </div>

@endsection