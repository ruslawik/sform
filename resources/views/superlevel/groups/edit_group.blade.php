@extends('manage.layout')

@section('title', $title)

@section('content')

    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Группа</span> Группа - {{ $group['0']->group_name }}. Нажмите на имя студента чтобы добавить в группу
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="col-sm-5">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title mb-3">Все студенты</strong>
                    </div>
                    <div class="card-body">
                        <table id="bootstrap-data-table" class="table table-striped table-bordered">
                            <thead>
            	               <tr>
            		              <td>Имя</td>
                                  <td>Фамилия</td>
                                  <td>Добавить</td>
            	               </tr>
                            </thead>
                        <tbody>
            	@foreach ($all_students as $student)
            		<tr>
            			<td>{{ $student->name }}</td>
                        <td>{{ $student->surname}}</td>
                        <td><center><a href="/manage/add_student/{{$student->id}}/to_group/{{$group['0']->id}}"><i class="fa fa-plus" aria-hidden="true" style="color:green;"></i></a></center></td>
            		</tr>
            	@endforeach
                </tbody>
            </table>
            </div>
            </div>
        </div>
        <div class="col-sm-5">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title mb-3">Список группы</strong>
                    </div>
                    <div class="card-body">

                        <form action="{{ $action }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="group_id_hidden" value="{{ $group['0']->id }}">
                                <table>
                                  <tr>
                                    <td>
                                    Преподаватель:
                                    <select name="new_teacher_id" class="form-control">
                                        <option value="{{$group['0']->teacher_id}}">{{ $group['0']->teacher()->exists()?$group['0']->teacher->name." ".$group['0']->teacher->surname:"Не назначен" }}</option>
                                    @foreach ($teachers as $teacher)
                                        @if( $group['0']->teacher()->exists() != true )
                                        <option value="{{ $teacher->id }}">{{ $teacher->name." ".$teacher->surname }}</option>
                                        @endif
                                    @endforeach
                                        @if( $group['0']->teacher()->exists() == true )
                                            <option value="0">Не назначен</option>
                                        @endif
                                    </select>
                                    </td>
                                    <td>
                                        <br>
                                        &nbsp;
                                        <input type="submit" class="btn btn-success" value="Назначить">
                                    </td>
                                  </tr>
                                </table>
                            </form>
                            <hr>

                        <table class="table">
                            <thead>
                               <tr>
                                  <td>Имя</td>
                                  <td>Фамилия</td>
                                  <td>Удалить</td>
                               </tr>
                            </thead>
                            <tbody>
                            @foreach ($all_group_students as $student)
                            <tr>
                                <td>{{ $student->user->name }}</td>
                                <td>{{ $student->user->surname}}</td>
                                <td><center><a href="/manage/delete_student/{{$student->student_id}}/from_group/{{$student->group_id}}"><i class="fa fa-remove" aria-hidden="true" style="color:red;"></i></a></center></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>

@endsection

@section('datatable_js')
<script src="/manage_res/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="/manage_res/assets/js/lib/data-table/datatables-init.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
        } );
    </script>

@endsection