@extends('manage.layout')

@section('title', $title)

@section('content')
	
    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Редактор теста</span> {{ $test['0']->name }} - Нажмите "+" для добавления вопроса
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
         <form method="POST" action="{{ $action }}">
         	{{ csrf_field() }}
            <input type="hidden" name="quest_num" id="quest_num" value="{{ $q_num }}">

			<input type="hidden" name="ins_id" value="{{ $test['0']->id }}">

            <div id="questions">
            	@foreach ($questions as $q) 
            		<input type='hidden' id='quest_del_bdid_{{$q->local_id}}' name='quest_del_bdid_{{$q->local_id}}' value=''>
			    	<input type='hidden' name='quest_bdid_{{$q->local_id}}' value='{{$q->id}}'>
					<div class='q{{$q->local_id}}'><table><tr><td>Вопрос - {{$q->local_id}}<br>

						<textarea class='form-control' cols='90' rows=2 name='quest_{{$q->local_id}}' id='quest_{{$q->local_id}}'>{{$q->question}}</textarea><br></td><td>&nbsp;

							<a class='btn-xs btn-success rodnoy' onClick='add_ans_to({{$q->local_id}})'>Добавить ответ</a> |
					
					<input type='hidden' name='quest_{{$q->local_id}}_ans_num' id='quest_{{$q->local_id}}_ans_num' value='{{$q->answers->count() }}'>

					<a  href='#' style='cursor:pointer;' onClick='del_quest({{$q->local_id}})'><font color='red'>Удалить</font></a></td></tr><tr><td>
			     	<div id='anses_{{$q->local_id}}'>
			     	<?php $k = 1; ?>
            		@foreach ($q->answers as $answer)
            			<input type='hidden' id='ans_del_bdid_{{$q->id}}_{{$k}}' name='ans_del_bdid_{{$q->id}}_{{$k}}' value=''>
						<input type='hidden' name='ans_bdid_{{$q->id}}_{{$k}}' value='{{$answer->id}}'>
						<div id='q{{$q->id}}a{{$k}}'>
							<table><tr><td><input style='width:30px;' type='checkbox' name='quest_{{$q->id}}_ans_{{$k}}_right' class='form-control' {{ $answer->is_right?"checked":"" }}></td><td>
							<input type='text' class="form-control" value='{{$answer->answer}}' id='quest_{{$q->id}}_ans_{{$k}}' name='quest_{{$q->id}}_ans_{{$k}}'>
							</td><td>&nbsp;<a class="delete_answer" onClick='del_ans({{$q->id}}, {{$k}} );'><i class='fa fa-trash'></i></a>
							</td></tr></table>
						</div>
						<?php $k++; ?> 
            		@endforeach
            		</div></td></tr></table><hr></div>
            	@endforeach
            </div>
            <input type="submit" value="Сохранить" class="btn btn-success">
            <br><br><br><br>
        </form>
    </div>
@endsection

@section('editor_javascript')
    <script>
    function add_quest(){
		var q = '"';
		var code = jQuery("#questions").html();
		
		var quest_num = jQuery("#quest_num").val();
		quest_num++;
		
		var to_add = "<div class='q"+quest_num+"'><table><tr><td>Вопрос - "+quest_num+"<br><textarea class='form-control' cols='90' rows=2 name='quest_"+quest_num+"' id='quest_"+quest_num+"'></textarea><br>";
			to_add+= "<input type='hidden' name='quest_"+quest_num+"_ans_num' id='quest_"+quest_num+"_ans_num' value='1'>";
			to_add+= "<input type='hidden' name='quest_"+quest_num+"_inf_num' id='quest_"+quest_num+"_inf_num' value='1'>";
			to_add+= "</td><td>&nbsp;<a class='btn-xs btn-success rodnoy' onClick='add_ans_to("+q+quest_num+q+")'>Добавить ответ</a> | ";
			to_add+= "<a href='#' style='cursor:pointer;' onClick='del_quest("+q+quest_num+q+")'><font color='red'>Удалить</font></a></td></tr><tr><td>";
			to_add+= "<div id='anses_"+quest_num+"'></div></td></tr></table><hr></div>";
			//to_add+= "<a onClick='add_inf_to("+q+quest_num+q+")'>Добавить i</a> | ";
			//to_add+= "<a onClick='add_zan_to("+q+quest_num+q+")'>Добавить z</a> | ";
			
			
		
		jQuery("#questions").append(to_add);
		
		jQuery("#quest_num").val(quest_num);
	}
	
	function add_ans_to(quest_id){
		var ans_num = jQuery("#quest_"+quest_id+"_ans_num").val();
		ans_num++;
		var to_add = "<div id='q"+quest_id+"a"+ans_num+"'><table><tr><td><input style='width:30px;' type='checkbox' name='quest_"+quest_id+"_ans_"+ans_num+"_right' class='form-control'></td>";
	    	to_add+= "<td><input type='text' class='form-control' name='quest_"+quest_id+"_ans_"+ans_num+"'></td><td>&nbsp;<a class='delete_answer' onClick='del_ans("+quest_id+","+ans_num+");'><i class='fa fa-trash'></i></a></td></tr></table></div>";
		jQuery("#anses_"+quest_id).append(to_add);
		jQuery("#quest_"+quest_id+"_ans_num").val(ans_num);
	}
	function del_ans(qid, anid){
		jQuery("#q"+qid+"a"+anid).remove();
		jQuery("#ans_del_bdid_"+qid+"_"+anid).val("DEL");
		
	}
	function del_quest(quest_id){
		jQuery(".q"+quest_id).remove();
		jQuery("#quest_del_bdid_"+quest_id).val("DEL");
	}
	
	function add_inf_to(quest_id){
		var inf_num = jQuery("#quest_"+quest_id+"_inf_num").val();
		
		var to_add = "<textarea id='quest_"+quest_id+"_inf_"+inf_num+"' name='quest_"+quest_id+"_inf_"+inf_num+"'>id"+inf_num+"###</textarea><a id='quest_"+quest_id+"_inf_"+inf_num+"' onClick='del_inf("+quest_id+","+inf_num+")'>-X-</a>";
		jQuery("#i"+quest_id).append(to_add);
		
		inf_num++;
		jQuery("#quest_"+quest_id+"_inf_num").val(inf_num);
	}
	function del_inf(quest_id, infid){
		jQuery("#inf_del_bdid_"+quest_id+"_"+infid).val("DEL");
		jQuery("#quest_"+quest_id+"_inf_"+infid).remove();
		jQuery("#quest_"+quest_id+"_inf_"+infid).remove();
	}
	
	function update_cat(text, cat_id){
		$.ajax({
			url: "acts/update_cat.php",
			type: "POST",
			data: "cat_name="+text+"&cat_id="+cat_id,
			success:(function(html){
				alert(html);
			})
		});
	}
	jQuery(document).ready(function() {
    	jQuery(this).keydown(function(e) {
        	if(e.keyCode==107){
				e.preventDefault();
				add_quest();
			}
	    });
	});
    </script>

@endsection

