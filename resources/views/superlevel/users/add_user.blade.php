@extends('superlevel.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-warning">Внимание!</span> Вы можете добавлять новых пользователей и устанавливать их роли в системе.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Описание ролей</span> <br><b>Администратор</b> - может редактировать товары, добавлять новых пользователей, смотреть информацию о покупках<br><b>Клиент</b> - имеет доступ к товарам для покупки, может оплачивать их, смотреть свои данные
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

    <div class="col-lg-12">
            <form method="POST" action="{{ $action }}">
                {{ csrf_field() }}
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Добавить пользователя</strong>
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="input-group">
                                            <div class="input-group-addon">Введите почту</div>
                                            <input type="text" class="form-control col-sm-5" name="email">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Введите имя</div>
                                            <input type="text" class="form-control col-sm-5" name="name">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Введите фамилию</div>
                                            <input type="text" class="form-control col-sm-5" name="surname">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Придумайте пароль</div>
                                            <input type="text" class="form-control col-sm-5" name="password">
                            </div>
                            <br>
                            <div class="input-group">
                                            <div class="input-group-addon">Выберите роль</div>
                                            <select name="role" class="form-control col-sm-5">
                                                <option value="2">Клиент</option>
                                                <option value="1">Администратор</option>
                                            </select>
                            </div>
                            <button type="submit" class="btn btn-success" style="float:right;"><i class="fa fa-magic"></i>&nbsp; Добавить!</button>
                        </div>
                    </div>
            </form>
    </div>



@endsection