@extends('manage.layout')

@section('title', $title)

@section('content')

      <div class="col-sm-12">
                <div class="alert  alert-success alert-dismissible fade show" role="alert">
                  <span class="badge badge-pill badge-success">Инфо</span> Информация о заявке клиента {{ $app->name }} {{ $app->surname }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
    <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Личная информация</strong>
                </div>
                <div class="card-body">
                    <b>Имя: </b>{{ $app->name }}<br>
                    <b>Фамилия: </b>{{ $app->surname }}<br>
                    <b>Пол: </b>{{ $app->sex }}<br>
                    <b>Дата рождения: </b> {{ $app->born }}<br>
                    <b>Адрес: </b> {{ $app->address }}<br>
                    <b>Школа: </b>{{ $app->school }}<br>
                    <b>Класс: </b>{{ $app->sinif }}<br>
                    <b>Год выпуска: </b>{{ $app->outofdate }}<br>
                    <b>Телефон: </b>{{ $app->tel }}<br>
                    <hr>
                    <h4>Будущие планы</h4>
                    <hr>
                    <b>1 выбор профессии: </b> {{ $app->prof1 }} <br>
                    <b>2 выбор профессии: </b> {{ $app->prof2 }} <br>
                    <b>3 выбор профессии: </b> {{ $app->prof3 }} <br>
                    <b>Описание бизнеса: </b> {{ $app->your_business }}
                    <hr>
                    <h4>Ожидания от программы и работа с ней:</h4>
                    <hr>
                    <b>Определение признаков личности</b><br>
                    @if ($app->expect1 != "")
                        @foreach(explode(',', $app->expect1) as $ex) 
                            <i class="fa fa-check"></i> {{$expect1[$ex]}}<br>
                        @endforeach
                    @endif
                    <b>Профессиональная диагностика</b><br>
                    @if ($app->expect2 != "")
                        @foreach(explode(',', $app->expect2) as $ex) 
                            <i class="fa fa-check"></i> {{$expect2[$ex]}}<br>
                        @endforeach
                    @endif
                    <b>Практический опыт</b><br>
                    @if ($app->expect3 != "")
                        @foreach(explode(',', $app->expect3) as $ex) 
                            <i class="fa fa-check"></i> {{$expect3[$ex]}}<br>
                        @endforeach
                    @endif
                    <hr>
                    <b>По какой программе хотите работать:</b><br>
                    @if ($app->work != "")
                        @foreach(explode('w', $app->work) as $ex) 
                            @if (isset($work[$ex]))
                                <i class="fa fa-check"></i> {{$work[$ex]}}<br>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
    </div>

    <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Интересы</strong>
                </div>
                <div class="card-body">
                    <h4>Заинтересованность в баллах (от 1 до 5):</h4>
                    <hr>
                    <b>Математика: </b> {{ $app->math }} <br>
                    <b>Биология: </b> {{ $app->bio }} <br>
                    <b>Химия: </b> {{ $app->chem }} <br>
                    <b>Физика: </b> {{ $app->phys }} <br>
                    <b>Информатика: </b> {{ $app->it_interes }} <br>
                    <hr>
                    <b>Танцы: </b> {{ $app->dance }} <br>
                    <b>Пение: </b> {{ $app->sing }} <br>
                    <b>Игра на муз. инстр.: </b> {{ $app->music_inses }} <br>
                    <b>Публичные выступления: </b> {{ $app->public_speak }} <br>
                    <b>Писательское творчество: </b> {{ $app->write_creat }} <br>
                    <hr>
                    <h4>Знание языков:</h4>
                    <hr>
                    <table class="table">
                        <tr>
                            <td><b>Язык</b></td>
                            <td><b>Аудирование</b></td>
                            <td><b>Письмо</b></td>
                            <td><b>Чтение</b></td>
                            <td><b>Речь</b></td>
                        </tr>
                        <tr>
                            <td>Казахский</td>
                            <td>{{ $app->kazakh_language_aud }}</td>
                            <td>{{ $app->kazakh_language_write }}</td>
                            <td>{{ $app->kazakh_language_read }}</td>
                            <td>{{ $app->kazakh_language_speak }}</td>
                        </tr>
                        <tr>
                            <td>Русский</td>
                            <td>{{ $app->russian_language_aud }}</td>
                            <td>{{ $app->russian_language_write }}</td>
                            <td>{{ $app->russian_language_read }}</td>
                            <td>{{ $app->russian_language_speak }}</td>
                        </tr>
                        <tr>
                            <td>Английский</td>
                            <td>{{ $app->english_language_aud }}</td>
                            <td>{{ $app->english_language_write }}</td>
                            <td>{{ $app->english_language_read }}</td>
                            <td>{{ $app->english_language_speak }}</td>
                        </tr>
                        <tr>
                            <td>{{ $app->other_lang }}</td>
                            <td>{{ $app->other_language_aud }}</td>
                            <td>{{ $app->other_language_write }}</td>
                            <td>{{ $app->other_language_read }}</td>
                            <td>{{ $app->other_language_speak }}</td>
                        </tr>
                    </table>
                </div>
            </div>
    </div>
@endsection