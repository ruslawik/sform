@extends('teacher.layout')

@section('title', $title)

@section('content')

    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Тесты</span> Отображаются Ваши тесты
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            Название
            <form method="POST" action="{{ $action }}">
            	{{ csrf_field() }} 
            	<table class="table">
            		<tr>
            			<td>
            				<input type="text" name="new_test_name" class="form-control col-sm-12">
            			</td>
            			<td>
            				<input type="submit" value="Добавить" class="btn btn-success">
            			</td>
            		</tr>
            	</table>
            </form>
            <table class="table">
            	<tr>
            		<td>Название</td>
            		<td>Дата добавления</td>
                    <td>Удалить</td>
            	</tr>
            	@foreach ($all_tests as $test)
            		<tr>
            			<td><a href="/teacher/testedit/{{ $test->id }}">{{ $test->name }}</a></td>
            			<td>{{ $test->created_at }}</td>
                        <td><a href="/teacher/testedit/delete_test/{{$test->id}}"><i class='fa fa-trash'></i></a></td>
            		</tr>
            	@endforeach
            </table>
    </div>

@endsection