@extends('teacher.layout')

@section('title', $title)

@section('content')
	
    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Редактор теста</span> {{ $test['0']->name }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form action="{{ $action }}" method="POST">
                    {{ csrf_field() }}
			     <input type="hidden" name="ins_id" value="{{ $test['0']->id }}">
			     Максимальное время для выполнения (мин.):
                 <table>
                    <tr><td>
			     <input type="text" name="time_to_solve" value="{{ $test['0']->time_to_solve }}" class="form-control col-sm-9" ></td><td>
                 <input type="submit" value="Обновить" class="btn btn-success"></td>
                </tr>
                </table>
             </form>
			<hr>
            <div id="questions">
            	@foreach ($questions as $q)
					<div class='q{{$q->local_id}}'><a href="/teacher/testedit/question_edit/{{$q->id}}/{{ $test['0']->id }}">Вопрос - {{$q->local_id}}</a><br>
					</div>
            	@endforeach
            </div>
            <br>
       		<a href="/teacher/testedit/add_new_q_to/{{ $test['0']->id }}/{{ $q_num }}"><button class="btn btn-success">Добавить вопрос</button></a>
    </div>
@endsection

@section('editor_javascript')
    <script>
    	
    </script>

@endsection

