@extends('teacher.layout')

@section('title', $title)

@section('content')
    <div class="col-sm-6">
        <a href="/teacher/testedit/{{ $test['0']->id }}" class="btn btn-success" style="color:white;">Назад к вопросам</a>
        <hr>
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Тесты</span> Добавить вопрос в тест "{{$test['0']->name}}"
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form action="{{ $action }}" method="POST">
            	<input type="hidden" name="test_id" value="{{ $test_id }}">
            	<input type="hidden" name="q_num" value="{{ $q_num }}">
                {{ csrf_field() }}
                Введите вопрос:
                <textarea name="new_question" class="form-control" rows=5></textarea>
                <br>
                <input type="submit" value="Сохранить" class="form-control btn btn-success col-sm-3">
            </form>
    </div>

@endsection