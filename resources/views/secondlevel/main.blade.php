@extends('secondlevel.layout')

@section('title', $title)

@section('content')
	
	<style>
		p{
			color:black !important;
		}
	</style>

     @if ($errors->any())
    <div class="alert alert-danger">
        <ul style="margin-left:20px;">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
    <div class="col-sm-12">
            <div class="alert  alert-success alert-dismissible fade show" role="alert">
                <span class="badge badge-pill badge-success">Приветствуем!</span> Добро пожаловать, {{ $username }}!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
    </div>

     @if(Session::has('message'))
            <div class="alert alert-success">
                <b>{!! Session::get("message") !!}</b>
            </div>
    @endif

@endsection

@section('javascript')
<script>
    function show_add_comment_form(task_id, k){
        jQuery("#add_form").toggle('fast');
        jQuery('#task_id').val(task_id);
        jQuery('#task_id_span').html(k);
    }
</script>

<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace( 'summary-ckeditor' );
</script>
@endsection