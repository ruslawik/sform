@extends('teacher.layout')
@section('title', $title)
@section('content')
<div class="col-sm-12">
   <div class="alert  alert-success alert-dismissible fade show" role="alert">
      <span class="badge badge-pill badge-success">Информация о студенте</span> Отображается вся информация о студенте {{ $student['0']->name }} {{ $student['0']->surname }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
   </div>

   <div class="card">
      <div class="card-header">
         <strong class="card-title mb-3">Назначенные тестирования</strong>
      </div>
      <div class="card-body">
         <table class="table table-striped">
            <thead>
               <tr>
                  <td>Название тестирования</td>
                  <td>Крайний срок сдачи</td>
                  <td>Дата назначения</td>
               </tr>
            </thead>
            <tbody>
               @foreach($testings as $testing)
               <tr>
                  <td>
                     {{ $testing->test->name }}
                  </td>
                  <td>
                     @if($date <= $testing->deadline)
                     <font color="green"><b>{{ $testing->deadline }}</b></font>
                     @endif
                     @if($date > $testing->deadline)
                     <font color="red"><b>{{ $testing->deadline }}</b></font>
                     @endif
                  </td>
                  <td>{{ $testing->created_at }}</td>
               </tr>
               @endforeach
            </tbody>
         </table>
         @if ($testings->count() === 0)
         <div class="alert alert-success">
            Вам не назначено ни одного тестирования
         </div>
         @endif
      </div>
   </div>

   <div class="card">
      <div class="card-header">
         <strong class="card-title mb-3">Заданные домашние задания</strong>
      </div>
      <div class="card-body">
        <table class="table table-striped">
            <tr>
                <td>Текст задания</td>
                <td>Время отправки</td>
                <td>Приложенные файлы</td>
            </tr>
         @foreach ($hometasks as $hometask)
                    <tr>
                        <td>
                            {!! $hometask->hometask_text !!}
                        </td>
                        <td>
                            {{ $hometask->created_at }}
                        </td>
                        <td>
                            @if ($hometask->file1 != NULL )
                            <a href="/teacher/download/{{ $hometask->id }}/1">Скачать файл 1 </a><br> 
                            @endif
                            @if ($hometask->file2 != NULL )
                            <a href="/teacher/download/{{ $hometask->id }}/2">Скачать файл 2 </a><br>
                            @endif
                            @if ($hometask->file3 != NULL )
                            <a href="/teacher/download/{{ $hometask->id }}/3">Скачать файл 3 </a><br>
                            @endif
                        </td>
                        @endforeach
                    </tr>
                </table>
                        @if($hometasks->count() === 0)
                            Пока ничего нет...
                        @endif
      </div>
  </div>

  <div class="card">
      <div class="card-header">
         <strong class="card-title mb-3">Результаты тестов</strong>
      </div>
      <div class="card-body">
            @foreach ($results as $key => $result)
                        Завершенное тестирование <b> {{ $tests[$result['0']['test_id']]['0']->name}} </b>- {{ $key }}
                        <a href="/teacher/test_result/{{$student['0']->id}}/{{ $result['0']['test_id'] }}/{{ $key }}" class="btn-sm btn-success" style="color:white;cursor:pointer;"> Просмотреть</a>
                        <br><br>
            @endforeach
      </div>
  </div>



</div>
@endsection