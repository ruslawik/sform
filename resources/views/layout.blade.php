<!DOCTYPE html>
<html lang="zxx">

<head>
	<title>HaileyBury School Uniform</title>
	<link rel="icon" type="image/ico" href="http://www.haileybury.kz/favicon.ico"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta name="keywords" content="HaileyBury School Uniform" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<link href="/web/css/bootstrap.css" rel='stylesheet' type='text/css' />
	<link href="/web/css/login_overlay.css" rel='stylesheet' type='text/css' />
	<link href="/web/css/style6.css" rel='stylesheet' type='text/css' />
	<link rel="stylesheet" href="/web/css/shop.css" type="text/css" />
	<link rel="stylesheet" href="/web/css/owl.carousel.css" type="text/css" media="all">
	<link rel="stylesheet" href="/web/css/owl.theme.css" type="text/css" media="all">
	<link href="/web/css/style.css" rel='stylesheet' type='text/css' />
	<link href="/web/css/fontawesome-all.css" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Inconsolata:400,700" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800"
	    rel="stylesheet">
	 @yield('additional_css')
</head>

<body>
	<div class="banner-top container-fluid" id="home" style="background-color:#751938;">
		<!-- header -->
		<header>
			<div class="col-lg-2">
				<div id="logo_rus">
					<center>
					<h1 class="logo-w3layouts">
						<a class="navbar-brand" href="/">
							 <img src="http://www.haileybury.kz/images/logotype/symbol-white@2x.png" class="logo_image" style="margin-top:30px;"></a>
					</h1>
				</center>
				</div>
				<div class="col-md-3">
					<!---->
					<div class="overlay-login text-left">
						<button type="button" class="overlay-close1">
							<i class="fa fa-times" aria-hidden="true"></i>
						</button>
						<div class="wrap">
							<h5 class="text-center mb-4">Войти</h5>
							<div class="login p-5 bg-dark mx-auto mw-100">
								<form action="/login" method="post">
									{{csrf_field()}}
									<div class="form-group">
										<label class="mb-2">Ваша почта</label>
										<input type="text" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" required="">
										<small id="emailHelp" class="form-text text-muted"></small>
									</div>
									<div class="form-group">
										<label class="mb-2">Пароль</label>
										<input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="" required="">
									</div>
									<button type="submit" class="btn btn-primary submit mb-4">Войти</button>

								</form>
								@if (session('error'))
                        			<div class="alert alert-danger">
                            			{{ session('error') }}
                        			</div>
                    			@endif
							</div>
							<!---->
						</div>
					</div>
					<!---->
				</div>
			</div>
				
			<nav class="navbar navbar-expand-sm navbar-light bg-light top-header sm-2">

					<ul class="navbar-nav">
						<li class="nav-item">
							<a class="nav-link" href="/shop">Каталог</a>
						</li>
						@if (Auth::check())
								<a class="nav-link" href="/secondlevel/cabinet">
									Личный кабинет
								</a>
						@else
						<li class="nav-item button-log">
								<a class="nav-link btn-open" href="#">
									<span class="fa fa-user" aria-hidden="true"></span>&nbsp;Вход/Регистрация
								</a>
						</li>
						@endif
						<li class="nav-item" style="margin-top:10px !important; margin-left:15px;">
							<a class="lang_change" href="#">Ru |</a> <a class="lang_change" href="#">Kz |</a> <a class="lang_change" href="#">En</a>
						</li>
					</ul>
					
			</nav>

			

		</header>

		@yield('main_content')
		
	
	<!--footer -->
	<footer class="py-lg-5 py-3">
		<div class="container-fluid px-lg-5 px-3">
			<div class="row footer-top-w3layouts">
				<div class="col-lg-3 footer-grid-w3ls">
					<div class="footer-title">
						<h3>Haileybury Astana</h3>
					</div>
					<div class="footer-text">
						<p>Haileybury Астана является всемирно признанной IB школой, расположенной в центре столицы Казахстана. Мы гордимся высококлассной материально-технической базой, ориентированной на высококачественную международную учебную программу и обучение будущих лидеров. Более того, мы ценим и заботимся о каждом своем ученике, подготавливая их для поступления в ведущие мировые университеты.</p>
						<ul class="footer-social text-left mt-lg-4 mt-3">

							<li class="mx-2">
								<a href="#">
									<span class="fab fa-facebook-f"></span>
								</a>
							</li>
							<li class="mx-2">
								<a href="#">
									<span class="fab fa-twitter"></span>
								</a>
							</li>
							<li class="mx-2">
								<a href="#">
									<span class="fab fa-google-plus-g"></span>
								</a>
							</li>
							<li class="mx-2">
								<a href="#">
									<span class="fab fa-linkedin-in"></span>
								</a>
							</li>
							<li class="mx-2">
								<a href="#">
									<span class="fas fa-rss"></span>
								</a>
							</li>
							<li class="mx-2">
								<a href="#">
									<span class="fab fa-vk"></span>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 footer-grid-w3ls">
					
				</div>
				<div class="col-lg-3 footer-grid-w3ls">
					
				</div>
				<div class="col-lg-3 footer-grid-w3ls">
					<div class="footer-title">
						<h3>Контакты</h3>
					</div>
					<div class="footer-text">
						<p>Вы всегда можете обратиться по нижеуказанным контактам для получения помощи при покупке:</p>
						<p>+7 775 123 45 67</p>

					</div>
				</div>
			</div>
			<div class="copyright-w3layouts mt-4">
				<p class="copy-right text-center ">&copy; 2019
					<a href="http://haileybury.kz/"> HaileyBury </a>
				</p>
			</div>
		</div>
	</footer>
	<!-- //footer -->
	@yield("js")
	@if (session('error'))
		<script>
        	 $(".overlay-login").fadeToggle(200);
    	</script>
	@endif
</body>

</html>