@extends('layout')
@section('main_content')
</div>
    <br><br>
		<section class="banner-bottom-wthreelayouts py-lg-5 py-3">
			<div class="container-fluid">
				<div class="inner-sec-shop px-lg-4 px-3">
					<div class="row">
						<!-- product left -->
						<div class="side-bar col-lg-3">
							<div class="search-hotel">
								<h3 class="agileits-sear-head">Поиск</h3>
								<form action="#" method="post">
										<input class="form-control" type="search" name="search" placeholder="Название..." required="">
										<button class="btn1">
												<i class="fas fa-search"></i>
										</button>
										<div class="clearfix"> </div>
									</form>
							</div>
							<select class="form-control">
								<option>Мальчик</option>
								<option>Девочка</option>
							</select>
							<!-- //deals -->
						</div>
						<!-- //product left -->
						<!--/product right-->
						<div class="left-ads-display col-lg-9">
							<div class="wrapper_top_shop">
								<div class="row">
									<!-- /womens -->
									<div class="col-md-3 product-men women_two shop-gd">
										<div class="product-googles-info googles">
											<div class="men-pro-item">
												<div class="men-thumb-item">
													<img src="web/images/s1.jpg" class="img-fluid" alt="">
													<div class="men-cart-pro">
														<div class="inner-men-cart-pro">
															<a href="/single" class="link-product-add-cart"><i class="fa fa-search" aria-hidden="true"></i></a>
														</div>
													</div>
													
												</div>
												<div class="item-info-product">
													<div class="info-product-price">
														<div class="grid_meta">
															<div class="product_price">
																<h4>
																	<a href="/single">Farenheit (Grey)</a>
																</h4>
																<div class="grid-price mt-2">
																	<span class="money ">575.00 тенге</span>
																</div>
															</div>
															<span style="font-size:14px;">В наличии: 10</span>
														</div>
														<div class="googles single-item hvr-outline-out">
															<form action="#" method="post">
																<input type="hidden" name="cmd" value="_cart">
																<input type="hidden" name="add" value="1">
																<input type="hidden" name="googles_item" value="Farenheit">
																<input type="hidden" name="amount" value="575.00">
																<button type="submit" class="googles-cart pgoogles-cart">
																	<i class="fas fa-cart-plus"></i>
																</button>
															</form>

														</div>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-3 product-men women_two shop-gd">
										<div class="product-googles-info googles">
											<div class="men-pro-item">
												<div class="men-thumb-item">
													<img src="web/images/s2.jpg" class="img-fluid" alt="">
													<div class="men-cart-pro">
														<div class="inner-men-cart-pro">
															<a href="/single" class="link-product-add-cart"><i class="fa fa-search" aria-hidden="true"></i></a>
														</div>
													</div>
													
												</div>
												<div class="item-info-product">

													<div class="info-product-price">
														<div class="grid_meta">
															<div class="product_price">
																<h4>
																	<a href="/single">Opium (Grey)</a>
																</h4>
																<div class="grid-price mt-2">
																	<span class="money ">325.00 тенге</span>
																</div>
															</div>
															<span style="font-size:14px;">В наличии: 10</span>
														</div>
														<div class="googles single-item hvr-outline-out">
															<form action="#" method="post">
																<input type="hidden" name="cmd" value="_cart">
																<input type="hidden" name="add" value="1">
																<input type="hidden" name="googles_item" value="Opium (Grey)">
																<input type="hidden" name="amount" value="325.00">
																<button type="submit" class="googles-cart pgoogles-cart">
																	<i class="fas fa-cart-plus"></i>
																</button>

															</form>

														</div>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-3 product-men women_two shop-gd">
										<div class="product-googles-info googles">
											<div class="men-pro-item">
												<div class="men-thumb-item">
													<img src="web/images/s3.jpg" class="img-fluid" alt="">
													<div class="men-cart-pro">
														<div class="inner-men-cart-pro">
															<a href="/single" class="link-product-add-cart"><i class="fa fa-search" aria-hidden="true"></i></a>
														</div>
													</div>
													
												</div>
												<div class="item-info-product">

													<div class="info-product-price">
														<div class="grid_meta">
															<div class="product_price">
																<h4>
																	<a href="/single">Kenneth Cole</a>
																</h4>
																<div class="grid-price mt-2">
																	<span class="money ">575.00 тенге</span>
																</div>
															</div>
															<span style="font-size:14px;">В наличии: 10</span>
														</div>
														<div class="googles single-item hvr-outline-out">
															<form action="#" method="post">
																<input type="hidden" name="cmd" value="_cart">
																<input type="hidden" name="add" value="1">
																<input type="hidden" name="googles_item" value="Kenneth Cole">
																<input type="hidden" name="amount" value="575.00">
																<button type="submit" class="googles-cart pgoogles-cart">
																	<i class="fas fa-cart-plus"></i>
																</button>
															</form>

														</div>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-3 product-men women_two shop-gd">
										<div class="product-googles-info googles">
											<div class="men-pro-item">
												<div class="men-thumb-item">
													<img src="web/images/s4.jpg" class="img-fluid" alt="">
													<div class="men-cart-pro">
														<div class="inner-men-cart-pro">
															<a href="/single" class="link-product-add-cart"><i class="fa fa-search" aria-hidden="true"></i></a>
														</div>
													</div>
													
												</div>
												<div class="item-info-product">

													<div class="info-product-price">
														<div class="grid_meta">
															<div class="product_price">
																<h4>
																	<a href="/single">Farenheit Oval </a>
																</h4>
																<div class="grid-price mt-2">
																	<span class="money ">325.00 тенге</span>
																</div>
															</div>
															<span style="font-size:14px;">В наличии: 10</span>
														</div>
														<div class="googles single-item hvr-outline-out">
															<form action="#" method="post">
																<input type="hidden" name="cmd" value="_cart">
																<input type="hidden" name="add" value="1">
																<input type="hidden" name="googles_item" value="Farenheit Oval">
																<input type="hidden" name="amount" value="325.00">
																<button type="submit" class="googles-cart pgoogles-cart">
																	<i class="fas fa-cart-plus"></i>
																</button>
															</form>

														</div>
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row my-lg-3 my-0">
										<!-- /womens -->
										<div class="col-md-3 product-men women_two shop-gd">
											<div class="product-googles-info googles">
												<div class="men-pro-item">
													<div class="men-thumb-item">
														<img src="web/images/m1.jpg" class="img-fluid" alt="">
														<div class="men-cart-pro">
															<div class="inner-men-cart-pro">
																<a href="/single" class="link-product-add-cart"><i class="fa fa-search" aria-hidden="true"></i></a>
															</div>
														</div>
														
													</div>
													<div class="item-info-product">
					
														<div class="info-product-price">
															<div class="grid_meta">
																<div class="product_price">
																	<h4>
																		<a href="/single">Aislin Wayfarer </a>
																	</h4>
																	<div class="grid-price mt-2">
																		<span class="money ">775.00 тенге</span>
																	</div>
																</div>
																<span style="font-size:14px;">В наличии: 10</span>
															</div>
															<div class="googles single-item hvr-outline-out">
																<form action="#" method="post">
																	<input type="hidden" name="cmd" value="_cart">
																	<input type="hidden" name="add" value="1">
																	<input type="hidden" name="googles_item" value="Aislin Wayfarer">
																	<input type="hidden" name="amount" value="775.00">
																	<button type="submit" class="googles-cart pgoogles-cart">
																		<i class="fas fa-cart-plus"></i>
																	</button>
																</form>
					
															</div>
														</div>
														<div class="clearfix"></div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3 product-men women_two shop-gd">
											<div class="product-googles-info googles">
												<div class="men-pro-item">
													<div class="men-thumb-item">
														<img src="web/images/m2.jpg" class="img-fluid" alt="">
														<div class="men-cart-pro">
															<div class="inner-men-cart-pro">
																<a href="/single" class="link-product-add-cart"><i class="fa fa-search" aria-hidden="true"></i></a>
															</div>
														</div>
														
													</div>
													<div class="item-info-product">
					
														<div class="info-product-price">
															<div class="grid_meta">
																<div class="product_price">
																	<h4>
																		<a href="/single">Azmani Round </a>
																	</h4>
																	<div class="grid-price mt-2">
																		<span class="money ">725.00 тенге</span>
																	</div>
																</div>
																<span style="font-size:14px;">В наличии: 10</span>
															</div>
															<div class="googles single-item hvr-outline-out">
																<form action="#" method="post">
																	<input type="hidden" name="cmd" value="_cart">
																	<input type="hidden" name="add" value="1">
																	<input type="hidden" name="googles_item" value="Azmani Round">
																	<input type="hidden" name="amount" value="725.00">
																	<button type="submit" class="googles-cart pgoogles-cart">
																		<i class="fas fa-cart-plus"></i>
																	</button>
																</form>
															</div>
														</div>
														<div class="clearfix"></div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3 product-men women_two shop-gd">
											<div class="product-googles-info googles">
												<div class="men-pro-item">
													<div class="men-thumb-item">
														<img src="web/images/m3.jpg" class="img-fluid" alt="">
														<div class="men-cart-pro">
															<div class="inner-men-cart-pro">
																<a href="/single" class="link-product-add-cart"><i class="fa fa-search" aria-hidden="true"></i></a>
															</div>
														</div>
														
													</div>
													<div class="item-info-product">
					
														<div class="info-product-price">
															<div class="grid_meta">
																<div class="product_price">
																	<h4>
																		<a href="/single">Farenheit Wayfarer</a>
																	</h4>
																	<div class="grid-price mt-2">
																		<span class="money ">475.00 тенге</span>
																	</div>
																</div>
																<span style="font-size:14px;">В наличии: 10</span>
															</div>
															<div class="googles single-item hvr-outline-out">
																<form action="#" method="post">
																	<input type="hidden" name="cmd" value="_cart">
																	<input type="hidden" name="add" value="1">
																	<input type="hidden" name="googles_item" value="Farenheit Wayfarer">
																	<input type="hidden" name="amount" value="475.00">
																	<button type="submit" class="googles-cart pgoogles-cart">
																		<i class="fas fa-cart-plus"></i>
																	</button>
																</form>
					
															</div>
														</div>
														<div class="clearfix"></div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-3 product-men women_two shop-gd">
											<div class="product-googles-info googles">
												<div class="men-pro-item">
													<div class="men-thumb-item">
														<img src="web/images/m4.jpg" class="img-fluid" alt="">
														<div class="men-cart-pro">
															<div class="inner-men-cart-pro">
																<a href="/single" class="link-product-add-cart"><i class="fa fa-search" aria-hidden="true"></i></a>
															</div>
														</div>
														
													</div>
													<div class="item-info-product">
					
														<div class="info-product-price">
															<div class="grid_meta">
																<div class="product_price">
																	<h4>
																		<a href="/single">Fossil Wayfarer </a>
																	</h4>
																	<div class="grid-price mt-2">
																		<span class="money ">825.00 тенге</span>
																	</div>
																</div>
																<span style="font-size:14px;">В наличии: 10</span>
															</div>
															<div class="googles single-item hvr-outline-out">
																<form action="#" method="post">
																	<input type="hidden" name="cmd" value="_cart">
																	<input type="hidden" name="add" value="1">
																	<input type="hidden" name="googles_item" value="Fossil Wayfarer">
																	<input type="hidden" name="amount" value="825.00">
																	<button type="submit" class="googles-cart pgoogles-cart">
																		<i class="fas fa-cart-plus"></i>
																	</button>
																</form>
					
															</div>
														</div>
														<div class="clearfix"></div>
													</div>
												</div>
											</div>
										</div>
										<!-- /mens -->
									</div>
									<div class="row">
											<!-- /womens -->
											<div class="col-md-3 product-men women_two shop-gd">
												<div class="product-googles-info googles">
													<div class="men-pro-item">
														<div class="men-thumb-item">
															<img src="web/images/s1.jpg" class="img-fluid" alt="">
															<div class="men-cart-pro">
																<div class="inner-men-cart-pro">
																	<a href="/single" class="link-product-add-cart"><i class="fa fa-search" aria-hidden="true"></i></a>
																</div>
															</div>
															
														</div>
														<div class="item-info-product">
															<div class="info-product-price">
																<div class="grid_meta">
																	<div class="product_price">
																		<h4>
																			<a href="/single">Farenheit (Grey)</a>
																		</h4>
																		<div class="grid-price mt-2">
																			<span class="money ">575.00 тенге</span>
																		</div>
																	</div>
																	<span style="font-size:14px;">В наличии: 10</span>
																</div>
																<div class="googles single-item hvr-outline-out">
																	<form action="#" method="post">
																		<input type="hidden" name="cmd" value="_cart">
																		<input type="hidden" name="add" value="1">
																		<input type="hidden" name="googles_item" value="Farenheit">
																		<input type="hidden" name="amount" value="575.00">
																		<button type="submit" class="googles-cart pgoogles-cart">
																			<i class="fas fa-cart-plus"></i>
																		</button>
																	</form>
		
																</div>
															</div>
															<div class="clearfix"></div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-3 product-men women_two shop-gd">
												<div class="product-googles-info googles">
													<div class="men-pro-item">
														<div class="men-thumb-item">
															<img src="web/images/s2.jpg" class="img-fluid" alt="">
															<div class="men-cart-pro">
																<div class="inner-men-cart-pro">
																	<a href="/single" class="link-product-add-cart"><i class="fa fa-search" aria-hidden="true"></i></a>
																</div>
															</div>
															
														</div>
														<div class="item-info-product">
		
															<div class="info-product-price">
																<div class="grid_meta">
																	<div class="product_price">
																		<h4>
																			<a href="/single">Opium (Grey)</a>
																		</h4>
																		<div class="grid-price mt-2">
																			<span class="money ">325.00 тенге</span>
																		</div>
																	</div>
																	<span style="font-size:14px;">В наличии: 10</span>
																</div>
																<div class="googles single-item hvr-outline-out">
																	<form action="#" method="post">
																		<input type="hidden" name="cmd" value="_cart">
																		<input type="hidden" name="add" value="1">
																		<input type="hidden" name="googles_item" value="Opium (Grey)">
																		<input type="hidden" name="amount" value="325.00">
																		<button type="submit" class="googles-cart pgoogles-cart">
																			<i class="fas fa-cart-plus"></i>
																		</button>
																	</form>
		
																</div>
															</div>
															<div class="clearfix"></div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-3 product-men women_two shop-gd">
												<div class="product-googles-info googles">
													<div class="men-pro-item">
														<div class="men-thumb-item">
															<img src="web/images/s3.jpg" class="img-fluid" alt="">
															<div class="men-cart-pro">
																<div class="inner-men-cart-pro">
																	<a href="/single" class="link-product-add-cart"><i class="fa fa-search" aria-hidden="true"></i></a>
																</div>
															</div>
															
														</div>
														<div class="item-info-product">
		
															<div class="info-product-price">
																<div class="grid_meta">
																	<div class="product_price">
																		<h4>
																			<a href="/single">Kenneth Cole</a>
																		</h4>
																		<div class="grid-price mt-2">
																			<span class="money ">575.00 тенге</span>
																		</div>
																	</div>
																	<span style="font-size:14px;">В наличии: 10</span>
																</div>
																<div class="googles single-item hvr-outline-out">
																	<form action="#" method="post">
																		<input type="hidden" name="cmd" value="_cart">
																		<input type="hidden" name="add" value="1">
																		<input type="hidden" name="googles_item" value="Kenneth Cole">
																		<input type="hidden" name="amount" value="575.00">
																		<button type="submit" class="googles-cart pgoogles-cart">
																			<i class="fas fa-cart-plus"></i>
																		</button>
																	</form>
		
																</div>
															</div>
															<div class="clearfix"></div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-3 product-men women_two shop-gd">
												<div class="product-googles-info googles">
													<div class="men-pro-item">
														<div class="men-thumb-item">
															<img src="web/images/s4.jpg" class="img-fluid" alt="">
															<div class="men-cart-pro">
																<div class="inner-men-cart-pro">
																	<a href="/single" class="link-product-add-cart"><i class="fa fa-search" aria-hidden="true"></i></a>
																</div>
															</div>
															
														</div>
														<div class="item-info-product">
		
															<div class="info-product-price">
																<div class="grid_meta">
																	<div class="product_price">
																		<h4>
																			<a href="/single">Farenheit Oval </a>
																		</h4>
																		<div class="grid-price mt-2">
																			<span class="money ">325.00 тенге</span>
																		</div>
																	</div>
																	<span style="font-size:14px;">В наличии: 10</span>
																</div>
																<div class="googles single-item hvr-outline-out">
																	<form action="#" method="post">
																		<input type="hidden" name="cmd" value="_cart">
																		<input type="hidden" name="add" value="1">
																		<input type="hidden" name="googles_item" value="Farenheit Oval">
																		<input type="hidden" name="amount" value="325.00">
																		<button type="submit" class="googles-cart pgoogles-cart">
																			<i class="fas fa-cart-plus"></i>
																		</button>
																	</form>
		
																</div>
															</div>
															<div class="clearfix"></div>
														</div>
													</div>
												</div>
											</div>
										</div>
							</div>
						</div>
						<!--//product right-->
					</div>
					<!--/slide-->
					<!--//slider-->
				</div>
			</div>
		</section>
		<!--footer -->
@endsection

@section("js")

		<!--jQuery-->
		<script src="/web/js/jquery-2.2.3.min.js"></script>
		<!-- newsletter modal -->
		<!--search jQuery-->
		<script src="/web/js/modernizr-2.6.2.min.js"></script>
		<script src="/web/js/classie-search.js"></script>
		<script src="/web/js/demo1-search.js"></script>
		<!--//search jQuery-->
		<!-- cart-js -->
		<script src="/web/js/minicart.js"></script>
		<script>
			googles.render();

			googles.cart.on('googles_checkout', function (evt) {
				var items, len, i;

				if (this.subtotal() > 0) {
					items = this.items();

					for (i = 0, len = items.length; i < len; i++) {}
				}
			});
		</script>
		<!-- //cart-js -->
		<script>
			$(document).ready(function () {
				$(".button-log a").click(function () {
					$(".overlay-login").fadeToggle(200);
					$(this).toggleClass('btn-open').toggleClass('btn-close');
				});
			});
			$('.overlay-close1').on('click', function () {
				$(".overlay-login").fadeToggle(200);
				$(".button-log a").toggleClass('btn-open').toggleClass('btn-close');
				open = false;
			});
		</script>
		<!-- carousel -->
		<!-- price range (top products) -->
		<script src="/web/js/jquery-ui.js"></script>
		<script>
			//<![CDATA[ 
			$(window).load(function () {
				$("#slider-range").slider({
					range: true,
					min: 0,
					max: 50000,
					values: [5000, 50000],
					slide: function (event, ui) {
						$("#amount").val(ui.values[0] + " - " + (ui.values[1]));
					}
				});
				$("#amount").val("" + $("#slider-range").slider("values", 0) + " - " + $("#slider-range").slider("values", 1));

			}); //]]>
		</script>
		<!-- //price range (top products) -->

		<script src="/web/js/owl.carousel.js"></script>
		<script>
			$(document).ready(function () {
				$('.owl-carousel').owlCarousel({
					loop: true,
					margin: 10,
					responsiveClass: true,
					responsive: {
						0: {
							items: 1,
							nav: true
						},
						600: {
							items: 2,
							nav: false
						},
						900: {
							items: 3,
							nav: false
						},
						1000: {
							items: 4,
							nav: true,
							loop: false,
							margin: 20
						}
					}
				})
			})
		</script>

		<!-- //end-smooth-scrolling -->


		<!-- dropdown nav -->
		<script>
			$(document).ready(function () {
				$(".dropdown").hover(
					function () {
						$('.dropdown-menu', this).stop(true, true).slideDown("fast");
						$(this).toggleClass('open');
					},
					function () {
						$('.dropdown-menu', this).stop(true, true).slideUp("fast");
						$(this).toggleClass('open');
					}
				);
			});
		</script>
		<!-- //dropdown nav -->
		<script src="/web/js/move-top.js"></script>
    <script src="/web/js/easing.js"></script>
    <script>
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event) {
                event.preventDefault();
                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 900);
            });
        });
        function show_lang(){
        	$(".languages").toggle('down');
        }
    </script>
    <script>
        $(document).ready(function() {
            /*
            						var defaults = {
            							  containerID: 'toTop', // fading element id
            							containerHoverID: 'toTopHover', // fading element hover id
            							scrollSpeed: 1200,
            							easingType: 'linear' 
            						 };
            						*/

            $().UItoTop({
                easingType: 'easeOutQuart'
            });

        });
    </script>
    <!--// end-smoth-scrolling -->

		<script src="/web/js/bootstrap.js"></script>
		<!-- js file -->
@endsection