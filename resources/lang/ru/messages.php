<?php

return [
	/*Main page messages*/
    'manage_main_welcome' => 'Добро пожаловать',

    /*Menu links*/
    'menu_main_page' => 'Главная',
    'menu_business_processes' => 'Бизнес процессы',
    'menu_tasks' => 'Задачи',
    'menu_all_tasks' => 'Все задачи',
    'menu_add_a_task' => 'Добавить задачу',
    'menu_clients' => 'Клиенты',
    'menu_applications' => 'Заявки',
    'menu_all_clients' => 'Все клиенты',
    'menu_add_a_client' => 'Добавить клиента',
    'menu_know_base' => 'База знаний',
    'menu_info' => 'Информация',
    'menu_org_structure' => 'Структура организации',
    'menu_add_a_know_base_row' => 'Добавить запись',
    'menu_finances' => 'Финансы',
    'menu_finance_outcome' => 'Расходы',
    'menu_finance_income' => 'Доходы',
    'menu_finance_analytics' => 'Аналитика',


    /*Структура организации*/
    'title_org_structure' => 'Структура организации',


];

?>