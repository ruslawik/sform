<?php

return [
	/*Main page messages*/
    'manage_main_welcome' => 'Glad to see you',

    /*Menu links*/
    'menu_main_page' => 'Main',
    'menu_business_processes' => 'Business processes',
    'menu_tasks' => 'Tasks',
    'menu_all_tasks' => 'All tasks',
    'menu_add_a_task' => 'Add a task',
    'menu_clients' => 'Clients',
    'menu_applications' => 'Applications',
    'menu_all_clients' => 'All clients',
    'menu_add_a_client' => 'Add a client',
    'menu_know_base' => 'Knowledge base',
    'menu_info' => 'Information',
    'menu_org_structure' => 'Organisation structure',
    'menu_add_a_know_base_row' => 'Add info',
    'menu_finances' => 'Finance',
    'menu_finance_outcome' => 'Outcome',
    'menu_finance_income' => 'Income',
    'menu_finance_analytics' => 'Analytics',
];

?>