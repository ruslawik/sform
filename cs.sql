-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Янв 13 2019 г., 08:50
-- Версия сервера: 10.1.30-MariaDB
-- Версия PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `cs`
--

-- --------------------------------------------------------

--
-- Структура таблицы `answers`
--

CREATE TABLE `answers` (
  `id` int(10) NOT NULL,
  `answer` text CHARACTER SET utf8mb4,
  `question_id` int(10) NOT NULL,
  `is_right` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `answers`
--

INSERT INTO `answers` (`id`, `answer`, `question_id`, `is_right`, `created_at`, `updated_at`) VALUES
(1, 'пример ответа 1', 1, 0, '2018-08-31 20:41:19', '2018-08-31 20:41:19'),
(2, 'верный ответ 2', 1, 1, '2018-08-31 20:41:19', '2018-08-31 20:41:19'),
(17, 'Test (right)', 7, 1, '2018-09-03 11:48:56', '2018-09-03 11:48:56'),
(18, 'Test (wrong)', 7, 0, '2018-09-03 11:48:56', '2018-09-03 11:48:56'),
(19, 'opened', 8, 0, '2018-09-03 12:10:11', '2018-09-04 13:02:18'),
(20, 'began', 8, 0, '2018-09-03 12:10:11', '2018-09-04 13:02:18'),
(21, 'arrived', 8, 0, '2018-09-03 12:10:11', '2018-09-04 13:02:18'),
(25, 'waits', 10, 0, '2018-09-03 12:11:47', '2018-09-04 13:02:18'),
(26, 'meets', 10, 0, '2018-09-03 12:11:47', '2018-09-04 13:02:18'),
(27, 'goes', 10, 0, '2018-09-03 12:11:47', '2018-09-04 13:02:18'),
(28, 'things', 11, 0, '2018-09-03 12:12:38', '2018-09-04 13:02:18'),
(29, 'ways', 11, 0, '2018-09-03 12:12:38', '2018-09-04 13:02:18'),
(30, 'kinds', 11, 0, '2018-09-03 12:12:38', '2018-09-04 13:02:18'),
(31, 'certain', 12, 0, '2018-09-03 12:13:07', '2018-09-04 13:02:18'),
(32, 'sure', 12, 0, '2018-09-03 12:13:07', '2018-09-04 13:02:18'),
(33, 'useful', 12, 0, '2018-09-03 12:13:07', '2018-09-04 13:02:18'),
(34, 'I didnít see them there. ', 13, 0, '2018-09-03 12:17:05', '2018-09-04 13:02:18'),
(35, 'favourite', 9, 0, '2018-09-03 14:39:57', '2018-09-07 12:21:00'),
(36, 'popular', 9, 0, '2018-09-03 14:39:57', '2018-09-07 12:21:00'),
(37, 'excellent', 9, 0, '2018-09-03 14:39:57', '2018-09-07 12:21:00'),
(38, 'It was a great time. ', 13, 0, '2018-09-03 14:39:57', '2018-09-04 13:02:18'),
(39, 'I was doing something else. ', 13, 0, '2018-09-03 14:39:57', '2018-09-04 13:02:18'),
(40, 'Have you been before? ', 14, 0, '2018-09-03 14:44:51', '2018-09-04 13:02:18'),
(41, 'Itís still quite early. ', 14, 0, '2018-09-03 14:44:51', '2018-09-04 13:02:18'),
(42, 'How long was it for? ', 14, 0, '2018-09-03 14:44:51', '2018-09-04 13:02:18'),
(43, 'it is not there', 15, 0, '2018-09-03 14:44:51', '2018-09-04 13:02:18'),
(44, 'wasn\'t it?', 15, 0, '2018-09-03 14:44:51', '2018-09-04 13:02:18'),
(45, 'I am not sure', 15, 0, '2018-09-03 14:44:51', '2018-09-04 13:02:18'),
(47, 'That\'s a pity', 16, 0, '2018-09-03 14:44:51', '2018-09-04 13:02:18'),
(48, 'It is not enough', 16, 0, '2018-09-03 14:44:51', '2018-09-04 13:02:18'),
(49, 'I hope so', 16, 0, '2018-09-03 14:44:51', '2018-09-04 13:02:18'),
(51, 'It is alright', 17, 0, '2018-09-03 14:44:51', '2018-09-04 13:02:18'),
(52, 'Yes, it is', 17, 0, '2018-09-03 14:44:51', '2018-09-04 13:02:18'),
(53, 'If you\'d like to', 17, 0, '2018-09-03 14:44:51', '2018-09-04 13:02:18'),
(55, 'Right', 18, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(56, 'Wrong ', 18, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(57, 'Doesn\'t say', 18, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(59, 'Right', 19, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(60, 'Wrong ', 19, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(61, 'Doesn\'t say', 19, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(63, 'Right', 20, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(64, 'Wrong ', 20, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(65, 'Doesn\'t say', 20, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(67, 'Right', 21, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(68, 'Wrong ', 21, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(69, 'Doesn\'t say', 21, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(71, 'Right', 22, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(72, 'Wrong ', 22, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(73, 'Doesn\'t say', 22, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(75, 'Right', 23, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(76, 'Wrong ', 23, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(77, 'Doesn\'t say', 23, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(79, 'Right', 24, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(80, 'Wrong ', 24, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(81, 'Doesn\'t say', 24, 0, '2018-09-03 14:54:06', '2018-09-04 13:02:18'),
(83, 'much', 25, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(84, 'a lot ', 25, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(85, 'little  ', 25, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(87, 'What Paris is like', 26, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(88, 'What’s like Paris?  ', 26, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(89, ' How’s Paris?  ', 26, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(91, 'at Saturday afternoons', 27, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(92, 'on Saturday afternoons ', 27, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(93, 'in Saturday afternoons  ', 27, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(95, 'her', 28, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(96, 'from', 28, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(97, 'by', 28, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(99, 'a red one', 29, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(100, 'played', 29, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(101, 'plays ', 29, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(103, 'To getting', 30, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(104, 'their', 30, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(105, 'his ', 30, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(108, 'Taking train what are you?  ', 31, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(109, 'Taking train what you are?  ', 31, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(110, 'What train are you taking?', 31, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(111, ' in Tuesday mornings  ', 32, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(112, ' on Tuesday mornings  ', 32, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(113, 'at Tuesday mornings  ', 32, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(114, 'it', 33, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(115, 'this', 33, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(116, 'her', 33, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(117, '', 34, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(118, '', 34, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(119, '', 34, 0, '2018-09-03 15:19:21', '2018-09-04 13:02:18'),
(121, 'from', 35, 0, '2018-09-04 09:59:00', '2018-09-04 13:02:18'),
(122, 'need', 35, 0, '2018-09-04 09:59:00', '2018-09-04 13:02:18'),
(123, 'must', 35, 0, '2018-09-04 09:59:00', '2018-09-04 13:02:18'),
(125, 'played', 36, 0, '2018-09-04 09:59:00', '2018-09-04 13:02:18'),
(126, 'plays ', 36, 0, '2018-09-04 09:59:00', '2018-09-04 13:02:18'),
(127, 'playing', 36, 0, '2018-09-04 09:59:00', '2018-09-04 13:02:18'),
(129, 'their', 37, 0, '2018-09-04 09:59:00', '2018-09-04 13:02:18'),
(130, 'his ', 37, 0, '2018-09-04 09:59:00', '2018-09-04 13:02:18'),
(131, 'its', 37, 0, '2018-09-04 09:59:00', '2018-09-04 13:02:18'),
(133, 'some ', 38, 0, '2018-09-04 09:59:00', '2018-09-04 13:02:18'),
(134, 'all', 38, 0, '2018-09-04 09:59:00', '2018-09-04 13:02:18'),
(135, 'enough', 38, 0, '2018-09-04 09:59:00', '2018-09-04 13:02:18'),
(137, 'more', 39, 0, '2018-09-04 10:02:05', '2018-09-04 13:02:18'),
(138, 'much', 39, 0, '2018-09-04 10:02:05', '2018-09-04 13:02:18'),
(139, 'most', 39, 0, '2018-09-04 10:02:05', '2018-09-04 13:02:18'),
(141, 'this', 40, 0, '2018-09-04 10:04:20', '2018-09-04 13:02:18'),
(142, 'it', 40, 0, '2018-09-04 10:04:20', '2018-09-04 13:02:18'),
(143, 'there', 40, 0, '2018-09-04 10:04:20', '2018-09-04 13:02:18'),
(154, '', 44, 0, '2018-09-04 13:02:18', '2018-09-04 13:02:18');

-- --------------------------------------------------------

--
-- Структура таблицы `calendar`
--

CREATE TABLE `calendar` (
  `id` int(10) NOT NULL,
  `teacher_id` int(10) NOT NULL,
  `hours` int(3) NOT NULL,
  `minutes` int(3) NOT NULL,
  `predmet_name` text CHARACTER SET utf8mb4 NOT NULL,
  `apta_kuni` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `calendar`
--

INSERT INTO `calendar` (`id`, `teacher_id`, `hours`, `minutes`, `predmet_name`, `apta_kuni`, `created_at`, `updated_at`) VALUES
(7, 15, 12, 9, 'пример предмета', 1, '2018-09-03 13:28:39', '2018-09-03 13:28:39');

-- --------------------------------------------------------

--
-- Структура таблицы `flows`
--

CREATE TABLE `flows` (
  `id` int(10) NOT NULL,
  `student_id` int(10) NOT NULL,
  `group_id` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `flows`
--

INSERT INTO `flows` (`id`, `student_id`, `group_id`, `created_at`, `updated_at`) VALUES
(1, 16, 1, '2018-09-03 11:27:12', '2018-09-03 11:27:12'),
(2, 16, 2, '2018-09-03 11:27:34', '2018-09-03 11:27:34');

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE `groups` (
  `id` int(10) NOT NULL,
  `group_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `teacher_id` int(10) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`id`, `group_name`, `teacher_id`, `created_at`, `updated_at`) VALUES
(1, 'Тестовая группа', 15, '2018-09-03 11:27:00', '2018-09-03 11:27:16'),
(2, 'Тестовая группа Асет', 14, '2018-09-03 11:27:27', '2018-09-03 11:27:35');

-- --------------------------------------------------------

--
-- Структура таблицы `hometasks`
--

CREATE TABLE `hometasks` (
  `id` int(10) NOT NULL,
  `student_id` int(10) NOT NULL,
  `hometask_text` longtext CHARACTER SET utf8mb4 NOT NULL,
  `file1` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `file2` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `file3` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `hometasks`
--

INSERT INTO `hometasks` (`id`, `student_id`, `hometask_text`, `file1`, `file2`, `file3`, `created_at`, `updated_at`) VALUES
(1, 16, '<p>Тест тест</p>', NULL, NULL, NULL, '2018-09-03 11:29:35', '2018-09-03 11:29:35');

-- --------------------------------------------------------

--
-- Структура таблицы `incomes`
--

CREATE TABLE `incomes` (
  `id` int(10) NOT NULL,
  `student_id` int(10) NOT NULL,
  `sum` int(10) NOT NULL,
  `who_took_id` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `incomes`
--

INSERT INTO `incomes` (`id`, `student_id`, `sum`, `who_took_id`, `created_at`, `updated_at`) VALUES
(1, 16, 50000, 1, '2018-09-12 09:40:47', '2018-09-12 09:40:47');

-- --------------------------------------------------------

--
-- Структура таблицы `now_passings`
--

CREATE TABLE `now_passings` (
  `id` int(10) NOT NULL,
  `student_id` int(10) NOT NULL,
  `test_id` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `now_passings`
--

INSERT INTO `now_passings` (`id`, `student_id`, `test_id`, `created_at`, `updated_at`) VALUES
(1, 16, 2, '2018-09-03 11:32:44', '2018-09-03 11:32:44'),
(2, 16, 3, '2018-09-03 11:33:42', '2018-09-03 11:33:42');

-- --------------------------------------------------------

--
-- Структура таблицы `questions_simple`
--

CREATE TABLE `questions_simple` (
  `id` int(10) NOT NULL,
  `question` text CHARACTER SET utf8mb4,
  `local_id` int(4) NOT NULL,
  `test_id` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `questions_simple`
--

INSERT INTO `questions_simple` (`id`, `question`, `local_id`, `test_id`, `created_at`, `updated_at`) VALUES
(1, 'пример вопроса', 1, 1, '2018-08-31 20:41:19', '2018-08-31 20:41:19'),
(7, 'Test', 2, 3, '2018-09-03 11:48:56', '2018-09-03 11:48:56'),
(8, 'Last month an internet cafe ............ near Ivanís house.', 1, 5, '2018-09-03 12:10:11', '2018-09-03 12:10:11'),
(9, 'The internet cafe quickly became ............ with Ivan and his friends', 2, 5, '2018-09-03 12:10:11', '2018-09-07 12:21:00'),
(10, 'Ivan often ............ his friends there after school. ', 3, 5, '2018-09-03 12:11:47', '2018-09-03 12:11:47'),
(11, 'The cafe has different ............ of computer games that they can play. ', 4, 5, '2018-09-03 12:12:38', '2018-09-03 12:12:38'),
(12, 'Ivan thinks there is a lot of ............ information on the internet. ', 5, 5, '2018-09-03 12:13:07', '2018-09-03 12:13:07'),
(13, 'Complete the five conversations.\r\nFor questions 6 & 10, mark A, B or C \r\n\r\n\r\n\r\nWhy didn\'t you come to the pool yesterday? ', 6, 5, '2018-09-03 12:17:05', '2018-09-03 14:39:57'),
(14, 'I have to go home now. ', 7, 5, '2018-09-03 14:44:51', '2018-09-03 14:44:51'),
(15, 'Whose phone is that? ', 8, 5, '2018-09-03 14:44:51', '2018-09-03 14:44:51'),
(16, 'There weren\'t any more tickets for the match. ', 9, 5, '2018-09-03 14:44:51', '2018-09-04 09:54:39'),
(17, 'Shall we play that new computer game? ', 10, 5, '2018-09-03 14:44:51', '2018-09-03 14:44:51'),
(18, 'Read the article about a young swimmer. \r\n\r\nAna hopes she will become an Olympic swimmer. ', 11, 5, '2018-09-03 14:54:06', '2018-09-04 09:54:39'),
(19, 'Ana knows that she is better at short races than long ones. ', 12, 5, '2018-09-03 14:54:06', '2018-09-03 14:54:06'),
(20, 'Ana has won a lot of swimming competitions. ', 13, 5, '2018-09-03 14:54:06', '2018-09-03 14:54:06'),
(21, 'It is difficult for Ana to make friends with other people who swim. ', 14, 5, '2018-09-03 14:54:06', '2018-09-03 14:54:06'),
(22, 'Ana likes doing the same things as other teenagers. \r\n', 15, 5, '2018-09-03 14:54:06', '2018-09-03 14:54:06'),
(23, 'Ana has met people from different countries at swimming competitions. ', 16, 5, '2018-09-03 14:54:06', '2018-09-03 14:54:06'),
(24, 'Ana prefers speaking to journalists to being on television. ', 17, 5, '2018-09-03 14:54:06', '2018-09-03 14:54:06'),
(25, 'We haven’t got ..... Champagne ', 18, 5, '2018-09-03 15:19:21', '2018-09-03 15:19:21'),
(26, 'Select the right answer', 19, 5, '2018-09-03 15:19:21', '2018-09-03 15:19:21'),
(27, ' I have Flamenco classes …… \r\n', 20, 5, '2018-09-03 15:19:21', '2018-09-03 15:19:21'),
(28, 'David is the boss, you need to speak to ….. ', 21, 5, '2018-09-03 15:19:21', '2018-09-03 15:19:21'),
(29, 'I wanted an orange car, but they only had ..... ', 22, 5, '2018-09-03 15:19:21', '2018-09-03 15:19:21'),
(30, 'We have to go to the supermarket ..... some bread and milk.', 23, 5, '2018-09-03 15:19:21', '2018-09-04 09:54:39'),
(31, 'Select the right answer', 24, 5, '2018-09-03 15:19:21', '2018-09-03 15:19:21'),
(32, 'She has her German classes …… ', 25, 5, '2018-09-03 15:19:21', '2018-09-03 15:19:21'),
(33, 'That file belongs to Patricia, give it to ', 26, 5, '2018-09-03 15:19:21', '2018-09-03 15:19:21'),
(34, 'Which pen do you want?', 27, 5, '2018-09-03 15:19:21', '2018-09-03 15:19:21'),
(35, 'Read the article about a circus. \r\nChoose the best word (A, B or C) for each space.\r\nFor questions 28 ñ 35, mark A, B or C ', 28, 5, '2018-09-04 09:59:00', '2018-09-04 09:59:00'),
(36, 'Choose the best word ', 29, 5, '2018-09-04 09:59:00', '2018-09-04 09:59:00'),
(37, 'Choose the best word ', 30, 5, '2018-09-04 09:59:00', '2018-09-04 09:59:00'),
(38, 'Choose the best word ', 31, 5, '2018-09-04 09:59:00', '2018-09-04 09:59:00'),
(39, 'Choose the best word ', 32, 5, '2018-09-04 10:02:05', '2018-09-04 10:02:05'),
(40, 'Choose the best word ', 33, 5, '2018-09-04 10:04:20', '2018-09-04 10:04:20'),
(44, '', 34, 5, '2018-09-04 12:42:07', '2018-09-04 12:42:07'),
(46, 'Первый вопрос', 1, 6, '2018-09-17 15:31:31', '2018-09-17 15:31:31');

-- --------------------------------------------------------

--
-- Структура таблицы `results`
--

CREATE TABLE `results` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `test_id` int(10) NOT NULL,
  `question_id` int(10) NOT NULL,
  `answer_id` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Структура таблицы `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `text` longtext CHARACTER SET utf8mb4 NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `author_id` int(10) NOT NULL,
  `file1` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `file2` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `file3` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Структура таблицы `testings`
--

CREATE TABLE `testings` (
  `id` int(10) NOT NULL,
  `student_id` int(10) NOT NULL,
  `test_id` int(10) NOT NULL,
  `deadline` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `testings`
--

INSERT INTO `testings` (`id`, `student_id`, `test_id`, `deadline`, `created_at`, `updated_at`) VALUES
(1, 16, 2, '2018-09-03 18:00:00', '2018-09-03 11:32:20', '2018-09-03 11:32:20'),
(2, 16, 3, '2018-09-03 15:00:00', '2018-09-03 11:33:07', '2018-09-03 11:33:07');

-- --------------------------------------------------------

--
-- Структура таблицы `tests`
--

CREATE TABLE `tests` (
  `id` int(10) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `time_to_solve` int(10) NOT NULL DEFAULT '20',
  `user_id` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `tests`
--

INSERT INTO `tests` (`id`, `name`, `time_to_solve`, `user_id`, `created_at`, `updated_at`) VALUES
(4, 'Placement Test', 20, 15, '2018-09-03 11:33:14', '2018-09-03 11:33:14'),
(5, 'Nova Education Placement Test for IELTS Preparation', 60, 15, '2018-09-03 11:39:02', '2018-09-04 13:02:18'),
(6, 'English level test', 20, 18, '2018-09-17 15:25:13', '2018-09-17 15:25:13');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(4) NOT NULL,
  `user_type` int(2) NOT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `user_type`, `login`, `name`, `surname`, `tel_number`, `password`, `is_active`, `updated_at`, `created_at`, `remember_token`) VALUES
(1, 1, 'admin', 'Манас', 'Нурбаев', NULL, '$2y$10$cnK8uXkKlSTiVB8JfPLgh.h3KPWib1yyHhn0OvNjhfqgcSpil/69a', 1, '2018-08-09', '2018-06-23', 'TiBsaQJKmxLplOcxJn8AJigOQ8eC0R9jd4yZgWBvdJTiNXbElZDTiCmqXjbw'),
(14, 3, 'asset', 'Асет', 'Аукенов', '+77086905371', '$2y$10$uj11Z/sKFJuaRwmsgGC3QOFoW77T/txraO.6feJvWd5ne5mppfaXi', 1, '2018-09-03', '2018-09-03', 'k9D3l8B5gCBlPwg66S1lsjC8VBo46mcQQ5926TY9YGipApv6avGOeo5Pddah'),
(15, 3, 'maralbek', 'Маралбек', 'Зейнуллин', '+77786882978', '$2y$10$ZixD0it29xlJ3EGy9Ev41./R4foimptH3a1RsMhZfliOPPV1bIe9e', 1, '2018-09-03', '2018-09-03', 'fh8NmvYoWbuhXldjFd3yZ0GPobCSSbSnc7ynjMpqiKaLY53NRWjIlGwL8qAR'),
(16, 2, 'student', 'Руслан', 'Хузин', NULL, '$2y$10$DmRbFHnAquNcVaime8t8huxSX4NsLhnYwXpVOxxR8F1IIVqKqNGB2', 1, '2018-09-03', '2018-09-03', 'Iz7KzmdCgyMxkUMyC9yGgclMGaJLFj3lpBrhvNCZDYPqUthowgUQ2mWTwtiw'),
(18, 3, 'trial_test', 'Trial', 'Test', '0000000000000', '$2y$10$ypAhwliP8A7IllS93TxV/eoYWHweCUo/pFNsNXTLpGbeGv7EOSdE2', 1, '2018-09-17', '2018-09-17', NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `calendar`
--
ALTER TABLE `calendar`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `flows`
--
ALTER TABLE `flows`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `hometasks`
--
ALTER TABLE `hometasks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `incomes`
--
ALTER TABLE `incomes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `now_passings`
--
ALTER TABLE `now_passings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `questions_simple`
--
ALTER TABLE `questions_simple`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `testings`
--
ALTER TABLE `testings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;

--
-- AUTO_INCREMENT для таблицы `calendar`
--
ALTER TABLE `calendar`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `flows`
--
ALTER TABLE `flows`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `hometasks`
--
ALTER TABLE `hometasks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `incomes`
--
ALTER TABLE `incomes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `now_passings`
--
ALTER TABLE `now_passings`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `questions_simple`
--
ALTER TABLE `questions_simple`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT для таблицы `results`
--
ALTER TABLE `results`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `testings`
--
ALTER TABLE `testings`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
